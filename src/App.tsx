import React, { ChangeEvent, FormEvent, useEffect, useState } from "react";
import Peer from "peerjs";
import "./App.css";

function App() {
  const initialId = localStorage.getItem("id") || undefined;
  const [dataConnection, setDataConnection] = useState<Peer.DataConnection>();
  const [messageText, setMessageText] = useState<string>();
  const [peer, setPeer] = useState<Peer>();
  const [peerId, setPeerId] = useState<string | undefined>(initialId);
  const [destId, setDestId] = useState<string>();

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (!peer || !destId) return;
    const dataConnection = peer.connect(destId);
    console.log(dataConnection.peerConnection);
    dataConnection.on("data", (data) => alert(data));
    console.log(dataConnection);
    setDataConnection(dataConnection);
  };

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setDestId(e.target.value);
  };

  const handleChangeMessage = (e: ChangeEvent<HTMLInputElement>) => {
    setMessageText(e.target.value);
  };

  const sendMessage = () => {
    if (!dataConnection) return;
    console.log(dataConnection);
    dataConnection.send(messageText);
  };

  useEffect(() => {
    const peerRaw = new Peer(initialId);
    peerRaw.on("open", (id: string) => {
      console.log(peerRaw);
      setPeerId(id);
      setPeer(peerRaw);
      if (!initialId) localStorage.setItem("id", id);
    });

    peerRaw.on("error", (error) => console.log(error));

    peerRaw.on("connection", (res) => {
      console.log(res);
      res.on("data", (data) => alert(data));
      setDataConnection(res);
    });
  }, [initialId]);

  useEffect(() => {});

  return (
    <>
      <form onSubmit={handleSubmit}>
        <label htmlFor="message">insert destination id</label>
        <input id="message" onChange={handleChange} />
        <button type="submit">send</button>
      </form>
      <input onChange={handleChangeMessage} />
      <button onClick={sendMessage}>send message</button>
      <p> your id is: {peerId}</p>
    </>
  );
}

export default App;
